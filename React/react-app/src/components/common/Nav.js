import React, { Component } from "react";
import "./Nav.css";
import { faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { NavLink } from "react-router-dom";

class Nav extends Component {
  render() {
    return (
      <div className="container">
        <nav className="navbar navbar-expand-md navbar-dark bg-light fixed-top">
          <NavLink to="/">
            <h4>Resturant</h4>
          </NavLink>

          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarsExampleDefault"
            aria-controls="navbarsExampleDefault"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item active">
                <NavLink to="/CalOrder">
                  <FontAwesomeIcon icon={faShoppingCart} />
                </NavLink>{" "}
                <span className="badge badge-pill badge-secondary">0</span>
              </li>
            </ul>
            <button className="btn btn-primary my-2 my-sm-0" type="submit">
              <NavLink to="/Login">Login</NavLink>
            </button>
          </div>
        </nav>
      </div>
    );
  }
}

export default Nav;
