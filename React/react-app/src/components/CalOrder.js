import React, { Component } from "react";
import { NavLink } from "react-router-dom";

class CalOrder extends Component {
  render() {
    return (
      <div className="container " style={{ marginTop: "70px" }}>
        <div className="card">
          <div className="card-header">
            <div className="row">
              <div className="col-md-6">Featured</div>
              <div className="col-md-6">
                <button type="submit" className="btn bg-light m-3">
                  Clear Shopping Cart
                </button>
              </div>
            </div>
          </div>
          <ul className="list-group list-group-flush">
            <li className="list-group-item">
              <div className="row">
                <div className="col-md-4">
                  {" "}
                  <img
                    className="rounded-circle m-2"
                    alt="100x100"
                    src="https://placehold.it/100x100"
                  />
                </div>
                <div className="col-md-4">
                  <label>name</label>{" "}
                  <button className="btn btn-primary m-2 " type="submit">
                    -
                  </button>
                </div>
                <div className="col-md-4">
                  <label>name</label>{" "}
                  <button className="btn btn-primary m-2 " type="submit">
                    +
                  </button>
                  <label>name</label>
                </div>
              </div>
            </li>
            <li className="list-group-item">
              {" "}
              <img
                className="rounded-circle m-2"
                alt="100x100"
                src="https://placehold.it/100x100"
              />
              <label>name</label>{" "}
              <button className="btn btn-primary m-2 " type="submit">
                -
              </button>
              <label>name</label>{" "}
              <button className="btn btn-primary m-2 " type="submit">
                +
              </button>
              <label>name</label>
            </li>
            <li className="list-group-item">
              {" "}
              <img
                className="rounded-circle m-2"
                alt="100x100"
                src="https://placehold.it/100x100"
              />
              <label>name</label>{" "}
              <button className="btn btn-primary m-2 " type="submit">
                -
              </button>
              <label>name</label>{" "}
              <button className="btn btn-primary m-2 " type="submit">
                +
              </button>
              <label>name</label>
            </li>
            <li className="list-group-item">
              <NavLink to="/CheckOut">
                <button type="submit" className="btn btn-primary m-3">
                  Check Out
                </button>
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

export default CalOrder;
