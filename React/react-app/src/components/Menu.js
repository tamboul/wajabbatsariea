import React from "react";
import "./Menu.css";
import { NavLink } from "react-router-dom";

function Minu() {
  return (
    <div className="container">
      <div className="row">
        <div className="col-md-8">
          <ul className="list-group">
            <li className="list-group-item bg-warning"> All Categories</li>
            <li className="list-group-item">
              <NavLink to="/">Home</NavLink>
            </li>
            <li className="list-group-item">
              <NavLink to="/News">News</NavLink>
            </li>
            <li className="list-group-item">
              <NavLink to="/Contact">Contact</NavLink>
            </li>
            <li className="list-group-item">
              <NavLink to="/About">About</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Minu;
