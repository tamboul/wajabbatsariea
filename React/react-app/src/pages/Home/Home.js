import React, { Component } from "react";
import ListProduct from "../../components/ListProduct";
import Menu from "../../components/Menu";
import Footer from "../../components/common/Footer";

class Home extends Component {
  render() {
    return (
      <div>
        <div className="container">
          <div className="container" style={{ marginTop: "70px" }}>
            <div className="row">
              <div className="col-md-4">
                <Menu />
              </div>
              <div className="col-md-8 ">
                {" "}
                <ListProduct />
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default Home;
