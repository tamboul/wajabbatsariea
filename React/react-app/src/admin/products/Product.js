import React, { Component } from "react";

class Product extends Component {
  render() {
    return (
      <div className="container" style={{ marginTop: "70px" }}>
        <button type="submit" className="btn btn-primary m-3">
          New Product
        </button>
        <input
          className="form-control "
          type="text"
          placeholder="Search"
          aria-label="Search"
        ></input>
      </div>
    );
  }
}

export default Product;
