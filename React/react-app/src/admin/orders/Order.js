import React, { Component } from "react";

class Order extends Component {
  render() {
    return (
      <div className="container m-auto" style={{ marginTop: "70px" }}>
        <h2>Thank you!</h2>
        <br></br>
        <p>We received your order</p>
      </div>
    );
  }
}

export default Order;
