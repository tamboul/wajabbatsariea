import React, { Component } from "react";
import Nav from "./components/common/Nav";

import { BrowserRouter, Switch, Route } from "react-router-dom";
import Home from "./pages/Home/Home";
import Login from "./components/Login";
import Cart from "./components/Cart";
import Order from "./admin/orders/Order";
import CalOrder from "./components/CalOrder";
import CheckOut from "./components/CheckOut";
import Product from "./admin/products/Product";
import Footer from "./components/common/Footer";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Nav />
          <Switch />
          <Route exact path="/" component={Home} />
          <Route path="/Login" component={Login} />
          <Route path="/Cart" component={Cart} />
          <Route path="/Order" component={Order} />
          <Route path="/CalOrder" component={CalOrder} />
          <Route path="/CheckOut" component={CheckOut} />
          <Route path="/Product" component={Product} />

          <Switch />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
